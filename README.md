# README #

Test repository for the company eiprice.


### Environment setup ###

* Docker version 19.03.8 and docker-compose version 1.17.1 are required on the machine.

### Initialization ###

* To start the project, just follow the steps below:
* 
1. [Clone the project](https://bitbucket.org/mauronascimento/teste_eiprice/src/master/)
2. Go to the project directory and give the command "docker-compose up --build"

### Testing the application ###

* After the application starts, it will answer by the local host or by the host '0.0.0.0'.
* After using a Postman application, make a post called at the endpoint "http://0.0.0.0:5000/info"
* Example of submitted body: <br />
```
{
"ean": ["28877362779", "28877495873", "885911251976", "885911230995"]
}
```
