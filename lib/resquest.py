import requests
import os


class HandlerRequest:

    def request(self, uri):
        return  requests.get(f'{os.getenv("PATH_GOOGLE")}{uri}')