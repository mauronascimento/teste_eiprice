import os
import csv

def handler_csv(ean, description, list):
    result = False
    count = 0

    with open(f'csv/{ean}.csv', mode='w+', encoding='utf-8', newline='') as csv_file:
        fieldnames = ['ean', 'description', 'value', 'vendor']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for value in list:
            if count % 2 == 0:
                preco = value
            else:
                result = writer.writerow({'ean': ean, 'description': description, 'value': preco, 'vendor': value})
            count = count + 1
    return result


def create_dir():
    if not os.path.isdir('csv'):
        os.makedirs('csv')