
LIST_NEGATION = ['', None, 'juros', 'x', 'Mostrando', '&gt', '%', '(', ' ', 'positivos']

DICT_REPLACE = {
    "LOJAS ON-LINE" : "",
    "Preço: do maior para o menor" : "",
    "Preço: do menor para o maior" : "",
    "Ordenar Padrão": "",
    "Ordenar": "",
    "Padrão": "",
    "Mais": "",
}


def handler_clean(list):
    list_value = []
    for value in list:
        if value and value not in LIST_NEGATION and not value.isdigit():
            list_value.append(value)
    return list_value


def multiple_replace(text, dic=DICT_REPLACE):
    pattern = "|".join(map(re.escape, dic.keys()))
    return re.sub(pattern, lambda m: dic[m.group()], text)
