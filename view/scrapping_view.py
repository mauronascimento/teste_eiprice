from flask import Response
from bs4 import BeautifulSoup
from lib.resquest import HandlerRequest
from lib.clean import handler_clean, multiple_replace
from lib.csv import handler_csv, create_dir
import logging
import json
import os
import re

handler_request = HandlerRequest()


class Scrapping:

    def __init__(self, content):
        self.content = content
        create_dir()

    def get_shopping(self):
        try:
            logging.debug('Starting google shopping search')

            for ean in self.content['ean']:
                req = handler_request.request(f'/search?tbm=shop&hl=pt-BR&source=hp&q={ean}')
                if req.status_code != 200:
                    response = {
                        "status": req.status_code,
                        "message": 'google shopping call error'
                    }
                    return Response(json.dumps(response), req.status_code)

                soup = BeautifulSoup(req.content, 'html.parser')

                href = soup.find("a", href=re.compile("/shopping/product/")).attrs['href']
                get_description = soup.find("div", class_='rgHvZc')
                description = re.compile(r'<[^>]+>').sub('', str(get_description))

                if not href or href is None:
                    response = {
                        "status": 400,
                        "message": 'no values ​​found in google shooping'
                    }
                    return Response(json.dumps(response), 400)

                self.get_detail(href, ean, description)
            response = {
                "status": 200,
                "message": 'ok'
            }

            return Response(json.dumps(response), 200)

        except Exception as err:
            logging.error(f'Generic Error: {err}')
            response = {
                "status": 400,
                "message": str(err)
            }
            return Response(json.dumps(response), 400)

    def get_detail(self, href, ean, description):

        try:
            logging.debug('Starting google detail search')
            req = handler_request.request(href)
            if req.status_code != 200:
                response = {
                    "status": req.status_code,
                    "message": 'google detail call error'
                }
                return Response(json.dumps(response), req.status_code)

            soup = BeautifulSoup(req.content, 'html.parser')

            table = soup.find('div', id='online')
            cleantext = multiple_replace(re.compile(r'<[^>]+>').sub(',', str(table)))
            list_split = cleantext.split(',')
            list_Send = handler_clean(list_split)
            handler_csv(ean, description, list_Send)

        except Exception as err:
            logging.error(f'Generic Error: {err}')
            response = {
                "status": 400,
                "message": str(err)
            }
            return Response(json.dumps(response), 400)

