FROM python:3.8.0-slim
COPY . /app
RUN apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install gcc -y \
    && apt-get clean

WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["app.py"]




