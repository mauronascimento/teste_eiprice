from flask import Flask, request, jsonify, Response
from flask_caching import Cache
from view.scrapping_view import Scrapping
import logging
import json

config = {
    "DEBUG": True,
    "CACHE_TYPE": "simple",
    "CACHE_DEFAULT_TIMEOUT": 300
}
app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)

@app.route('/', methods=['GET'])
def init():
    return 'Teste Eiprice'


@app.route('/info', methods=['POST'])
@cache.cached(timeout=60)
def info():

    logging.debug('Starting processing')

    if not request.is_json:
        logging.debug('invalid data')
        http_code = 400
        response = {
            "status": http_code,
            "message": "invalid data"
        }
        return Response(json.dumps(response), http_code)

    content = request.get_json()
    if 'ean' not in content:
        logging.debug('Key error: missing key "ean"')
        http_code = 400
        response = {
            "status": http_code,
            "message": "Key error: missing key 'ean'"
        }
        return Response(json.dumps(response), http_code)
    scrapping = Scrapping(content)
    return scrapping.get_shopping()


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    app.run(debug=True, host='0.0.0.0')